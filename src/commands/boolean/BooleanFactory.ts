import * as THREE from "three";
import c3d from '../../../build/Release/c3d.node';
import { MaterialOverride, TemporaryObject } from "../../editor/GeometryDatabase";
import * as visual from '../../visual_model/VisualModel';
import { composeMainName, vec2vec } from '../../util/Conversion';
import { GeometryFactory, PhantomInfo } from '../../command/GeometryFactory';
import { MoveParams } from "../translate/TranslateFactory";

interface BooleanLikeFactory extends GeometryFactory {
    solid?: visual.Solid;
    operationType: c3d.OperationType;

    // NOTE: These are hints for the factory to infer which operation
    isOverlapping: boolean;
    isSurface: boolean;
}

export interface BooleanParams {
    operationType: c3d.OperationType;
    mergingFaces: boolean;
    mergingEdges: boolean;
}

export class BooleanFactory extends GeometryFactory implements BooleanLikeFactory, BooleanParams {
    operationType: c3d.OperationType = c3d.OperationType.Difference;
    mergingFaces = true;
    mergingEdges = true;

    isOverlapping = false;
    isSurface = false;

    private _solid!: visual.Solid;
    solidModel!: c3d.Solid;
    get solid() { return this._solid }
    set solid(solid: visual.Solid) {
        this._solid = solid;
        this.solidModel = this.db.lookup(solid)
    }

    private _tools!: visual.Solid[];
    toolModels!: c3d.Solid[];
    get tools() { return this._tools }
    set tools(tools: visual.Solid[]) {
        this._tools = tools;
        this.toolModels = tools.map(t => this.db.lookup(t));
    }

    protected readonly names = new c3d.SNameMaker(composeMainName(c3d.CreatorType.BooleanSolid, this.db.version), c3d.ESides.SideNone, 0);
    protected _isOverlapping = false;

    async calculate() {
        const { solidModel, toolModels, names, mergingFaces, mergingEdges } = this;

        const flags = new c3d.MergingFlags();
        flags.SetMergingFaces(mergingFaces);
        flags.SetMergingEdges(mergingEdges);

        const { result } = await c3d.ActionSolid.UnionResult_async(solidModel, c3d.CopyMode.Copy, toolModels, c3d.CopyMode.Copy, this.operationType, true, flags, names, false);
        this._isOverlapping = true;
        return result;
    }

    protected get phantoms(): PhantomInfo[] {
        if (this.operationType === c3d.OperationType.Union) return [];

        let material: MaterialOverride;
        if (this.operationType === c3d.OperationType.Difference)
            material = phantom_red
        else if (this.operationType === c3d.OperationType.Intersect)
            material = phantom_green;
        else material = phantom_blue;

        const result = [];
        for (const phantom of this.toolModels) {
            result.push({ phantom, material })
        }
        if (this.operationType === c3d.OperationType.Intersect)
            result.push({ phantom: this.solidModel, material: phantom_blue });
        return result;
    }

    get originalItem() {
        return [this.solid, ...this.tools];
    }

    get shouldRemoveOriginalItemOnCommit() {
        return this._isOverlapping;
    }
}

export class UnionFactory extends BooleanFactory {
    operationType = c3d.OperationType.Union;
}

export class IntersectionFactory extends BooleanFactory {
    operationType = c3d.OperationType.Intersect;
}

export class DifferenceFactory extends BooleanFactory {
    operationType = c3d.OperationType.Difference;
}

export class MovingBooleanFactory extends BooleanFactory implements MoveParams {
    move = new THREE.Vector3();
    pivot = new THREE.Vector3();

    async calculate() {
        const tools = this.moveTools();
        const { solidModel, names, mergingFaces, mergingEdges } = this;

        const flags = new c3d.MergingFlags();
        flags.SetMergingFaces(mergingFaces);
        flags.SetMergingEdges(mergingEdges);

        const { result } = await c3d.ActionSolid.UnionResult_async(solidModel, c3d.CopyMode.Copy, tools, c3d.CopyMode.Copy, this.operationType, true, flags, names, false);
        this._isOverlapping = true;
        return result;
    }

    private moveTools() {
        let tools = [];
        const { move, toolModels } = this;
        if (move.manhattanLength() > 10e-6) {
            const transform = new c3d.TransformValues();
            transform.Move(vec2vec(move));
            const names = new c3d.SNameMaker(composeMainName(c3d.CreatorType.TransformedSolid, this.db.version), c3d.ESides.SideNone, 0);
            for (const tool of toolModels) {
                const transformed = c3d.ActionDirect.TransformedSolid(tool, c3d.CopyMode.Copy, transform, names);
                tools.push(transformed);
            }
        } else tools = toolModels;
        return tools;
    }

    private _phantoms?: Promise<TemporaryObject>[];
    protected addPhantoms(promises: Promise<TemporaryObject>[]) {
        if (this._phantoms === undefined) {
            const into: Promise<TemporaryObject>[] = [];
            super.addPhantoms(into);
            this._phantoms = into;
            DoNotCancelPhantomsOnUpdate: {
                for (const i of into) {
                    const uncancellable: Promise<TemporaryObject> = i.then(phantom => {
                        return {
                            underlying: phantom.underlying,
                            show() { phantom.show() },
                            cancel() { }
                        }
                    });
                    promises.push(uncancellable);
                }
            }
        }
        MovePhantomsOnUpdate: {
            const lastIndex = this._phantoms.length - 1;
            for (const [i, phantom] of this._phantoms.entries()) {
                // Don't move the original item phantom
                if (i === lastIndex && this.operationType === c3d.OperationType.Intersect) break;

                phantom.then(phantom => {
                    phantom.underlying.position.copy(this.move);
                });
            }
        }
    }

    protected cleanupTempsOnFinishOrCancel() {
        for (const temp of this.temps) temp.cancel();
        if (this._phantoms === undefined) return;
        CancelPhantomsForReal: {
            for (const temp of this._phantoms) temp.then(t => t.cancel());
        }
    }
}

export class MovingUnionFactory extends MovingBooleanFactory {
    operationType = c3d.OperationType.Union;
}

export class MovingIntersectionFactory extends MovingBooleanFactory {
    operationType = c3d.OperationType.Intersect;
}

export class MovingDifferenceFactory extends MovingBooleanFactory {
    operationType = c3d.OperationType.Difference;
}

export abstract class PossiblyBooleanFactory<GF extends GeometryFactory> extends GeometryFactory {
    protected abstract bool: BooleanLikeFactory;
    protected abstract fantom: GF;

    protected _phantom!: c3d.Solid;

    newBody = false;

    protected _operationType?: c3d.OperationType;
    get operationType() { return this._operationType ?? this.defaultOperationType }
    set operationType(operationType: c3d.OperationType) { this._operationType = operationType }
    get defaultOperationType() { return this.isSurface ? c3d.OperationType.Union : c3d.OperationType.Difference }

    protected _solid?: visual.Solid;
    protected model?: c3d.Solid;
    get solid() { return this._solid }
    set solid(solid: visual.Solid | undefined) {
        this._solid = solid;
        if (solid !== undefined) {
            this.bool.solid = solid;
            this.model = this.db.lookup(solid);
        }
    }

    protected _isOverlapping = false;
    get isOverlapping() { return this._isOverlapping }
    set isOverlapping(isOverlapping: boolean) {
        this._isOverlapping = isOverlapping;
        this.bool.isOverlapping = isOverlapping;
    }

    protected _isSurface = false;
    get isSurface() { return this._isSurface }
    set isSurface(isSurface: boolean) {
        this._isSurface = isSurface;
        this.bool.isSurface = isSurface;
    }

    protected async precomputeGeometry() {
        const phantom = await this.fantom.calculate() as c3d.Solid;
        this._phantom = phantom;
        if (this.solid === undefined) {
            this.isOverlapping = false;
            this.isSurface = false;
        } else {
            // FIXME: use MinimumSolidDistance which is faster
            const { isIntersection, intData } = await c3d.Action.IsSolidsIntersection_async(this.model!, new c3d.Matrix3D(), phantom, new c3d.Matrix3D(), true, false, false);
            this.isOverlapping = isIntersection;
            if (intData.length === 0) {
                this.isSurface = false;
            } else {
                this.isSurface = intData[0].IsSurface() && !intData[0].IsSolid();
            }
        }
    }

    async calculate() {
        await this.precomputeGeometry();
        if (this.isOverlapping && !this.newBody) {
            this.bool.operationType = this.operationType;
            const result = await this.bool.calculate() as c3d.Solid;
            return result;
        } else {
            return this._phantom;
        }
    }

    get phantoms(): PhantomInfo[] {
        if (this.solid === undefined) return [];
        if (this.newBody) return [];
        if (this.operationType === c3d.OperationType.Union) return [];
        if (!this.isOverlapping) return [];

        let material: MaterialOverride
        if (this.operationType === c3d.OperationType.Difference) material = phantom_red;
        else if (this.operationType === c3d.OperationType.Intersect) material = phantom_green;
        else material = phantom_blue;

        const phantom = this._phantom;

        return [{ phantom, material }];
    }

    get originalItem() { return this.solid }

    get shouldRemoveOriginalItemOnCommit() {
        return this.isOverlapping && this.solid !== undefined && !this.newBody;
    }
}

const mesh_red = new THREE.MeshBasicMaterial();
mesh_red.color.setHex(0xff0000);
mesh_red.opacity = 0.1;
mesh_red.transparent = true;
mesh_red.fog = false;
mesh_red.polygonOffset = true;
mesh_red.polygonOffsetFactor = 0.1;
mesh_red.polygonOffsetUnits = 1;

const surface_red = mesh_red.clone();
surface_red.side = THREE.DoubleSide;

const phantom_red: MaterialOverride = {
    mesh: mesh_red
}

const mesh_green = new THREE.MeshBasicMaterial();
mesh_green.color.setHex(0x00ff00);
mesh_green.opacity = 0.1;
mesh_green.transparent = true;
mesh_green.fog = false;
mesh_green.polygonOffset = true;
mesh_green.polygonOffsetFactor = 0.1;
mesh_green.polygonOffsetUnits = 1;

const phantom_green: MaterialOverride = {
    mesh: mesh_green
}


const mesh_blue = new THREE.MeshBasicMaterial();
mesh_blue.color.setHex(0x0000ff);
mesh_blue.opacity = 0.1;
mesh_blue.transparent = true;
mesh_blue.fog = false;
mesh_blue.polygonOffset = true;
mesh_blue.polygonOffsetFactor = 0.1;
mesh_blue.polygonOffsetUnits = 1;

const phantom_blue: MaterialOverride = {
    mesh: mesh_blue
}
